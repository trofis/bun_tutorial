import { Elysia } from "elysia";
import figlet from "figlet";
import { foo } from "data";

// GET
// Flower ()
// Flowers (limit)
// POST
// Fower(info)
// PUT (id, info)
// DELETE
//
console.log(`🦊 Elysia is running at on port ${app.server.port}...`);

const routes = (pathname: string) => {
  let response: Response;
  switch (pathname) {
    case "/":
      response = new Response(getFoo());
      break;
    case "/version":
      response = new Response(Bun.version);
      break;
    case "/figlet":
      response = new Response(figlet.textSync("Bun!"));
      break;
    case "/env":
      response = new Response(process.env.BAR);
      break;
    default:
      response = new Response("Default");
      break;
  }
  return response;
};

const getFoo = () => {
  return foo as string;
};

const app = new Elysia().get("/", () => "Hello Elysia").listen(8080);

const server = Bun.serve({
  port: 3000,
  fetch(req) {
    routes(req.url);

    const url = new URL(req.url);

    return routes(url.pathname);
  },
});

console.log(`Listening on http://localhost:${server.port} ...`);
